#!/usr/bin/make -f

# resolve DEB_VERSION
include /usr/share/dpkg/pkg-info.mk

# generate documentation unless nodoc requested
ifeq (,$(filter nodoc,$(DEB_BUILD_OPTIONS)))
CHANGELOGS = CHANGELOG.html CHANGELOG.txt
DOCS = README.html README.txt
endif

# use local fork of dh-cargo and cargo wrapper
PATH := $(CURDIR)/debian/dh-cargo/bin:$(PATH)
PERL5LIB = $(CURDIR)/debian/dh-cargo/lib
export PATH PERL5LIB

# resolve all direct entries in first "package..." section in TODO
VENDORLIBS = $(shell perl -nE \
 '/^(?:\s{2}[*]\s*(?:(package)|(\S+))|\s{4}[*]\s*(\S+))/ or next; \
 $$i++ if $$1; exit if $$i > 1 or $$2; say $$3 if $$3 and $$i;' \
 debian/TODO)

# generate cargo-checksum file
_mkchecksum = printf '{"package":"%s","files":{}}\n' \
 $$(sha256sum $(or $2,$(dir $1)Cargo.toml) | grep -Po '^\S+') > $1;

comma = ,
_mangle = perl -0777 -i -p \
 -e 's/$(1)\W+version = "\K\Q$(subst |,$(comma),$(2))"/$(subst |,$(comma),$(3))"/g;' \
 $(patsubst %,debian/vendorlibs/%/Cargo.toml,$(4))

%:
	dh $@ --buildsystem cargo

%.html: %.md
	cmark-gfm $< > $@

%.txt: %.md
	cmark-gfm --to plaintext $< > $@

execute_after_dh_auto_build: $(DOCS) $(CHANGELOGS)

override_dh_installdocs:
	dh_installdocs --all -- $(DOCS)

override_dh_installchangelogs:
	dh_installchangelogs -- $(CHANGELOGS)

# custom target unused during official build
get-vendorlibs:
# preserve and use pristine Cargo.lock
	[ -f Cargo.lock.orig ] || cp Cargo.lock Cargo.lock.orig
	cp -f Cargo.lock.orig Cargo.lock

# preserve needed crates
	cargo vendor --versioned-dirs debian/vendorlibs~
	rm -rf debian/vendorlibs
	mkdir debian/vendorlibs
	cd debian/vendorlibs~ && mv --target-directory=../vendorlibs $(VENDORLIBS)

# tolerate binary files in preserved code
	find debian/vendorlibs -type f ! -size 0 | perl -lne 'print if -B' \
		> debian/source/include-binaries

# accept older crates
	$(call _mangle,hashbrown,0.14,>= 0.12.3| <= 0.14,lru-0.12.1)

# update checksum of mangled crates
	set -e;\
	$(foreach x,\
		lru-0.12.1 \
		,\
		$(call _mkchecksum,debian/vendorlibs/$x/.cargo-checksum.json))

# preserve mangled Cargo.lock for use during build
	cp -f Cargo.lock debian/Cargo.lock

	$(eval GONE = $(filter-out 0,\
		$(shell grep -Fxc '      * package is missing' debian/TODO)))
	$(eval PENDING = $(filter-out 0,\
		$(shell grep -Fxc '      * package is pending' debian/TODO)))
	$(eval INCOMPLETE = $(filter-out 0,\
		$(shell grep -Fc '      * package lacks feature' debian/TODO)))
	$(eval BROKEN = $(filter-out 0,\
		$(shell grep -c '      \* package is .*broken' debian/TODO)))
	$(eval UNWANTED = $(filter-out 0,\
		$(shell grep -c '      \* package is unwanted' debian/TODO)))
	$(eval OLD = $(filter-out 0,\
		$(shell grep -Fxc '      * package is outdated' debian/TODO)))
	$(eval AHEAD = $(filter-out 0,\
		$(shell grep -Fxc '      * package is ahead' debian/TODO)))
	$(eval SNAPSHOT = $(filter-out 0,\
		$(shell grep -Fxc '      * package should cover snapshot' debian/TODO)))
	$(eval REASONS = $(strip\
		$(if $(GONE),$(GONE)_missing)\
		$(if $(PENDING),$(PENDING)_pending)\
		$(if $(BROKEN),$(BROKEN)_broken)\
		$(if $(UNWANTED),$(UNWANTED)_unwanted)\
		$(if $(INCOMPLETE),$(INCOMPLETE)_incomplete)\
		$(if $(OLD),$(OLD)_outdated)\
		$(if $(AHEAD),$(AHEAD)_ahead)\
		$(if $(SNAPSHOT),$(SNAPSHOT)_unreleased)))
	$(eval c = ,)
	$(eval EMBEDDED = $(if $(REASONS),\n    ($(subst _,$() ,$(subst $() ,$c_,$(REASONS))))))
	@CRATES=$$(ls -1 debian/vendorlibs | grep -c .);\
	perl -0777 -p -i \
		-e 's/FIXME: avoid most possible of embedded \K\d+ crates\n\h+[^\)\n]*\)/'"$$CRATES"' crates$(EMBEDDED)/;' \
		debian/changelog;\
	echo;\
	echo "DONE: embedding $$CRATES crates$(EMBEDDED)";\
	echo

	mv -f Cargo.lock.orig Cargo.lock
